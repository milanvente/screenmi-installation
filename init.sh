#!/bin/bash

ROOT=/home/screenmi/Documents/screenmi-setup
CYAN='\033[1;36m'
NC='\033[0m'

echo -e "${CYAN}[!] Create install folder ${NC}"
mkdir $ROOT
chown screenmi:screenmi -R $ROOT
cd $ROOT

echo -e "${CYAN}[!] Install required packages ${NC}"
apt-get update && apt-get upgrade -y
apt-get install git build-essential libev-dev libx11-dev libxfixes-dev libxi-dev pkg-config python3 htop curl unclutter -y

echo -e "${CYAN}[!] Disable screen time out ${NC}"
gsettings set org.gnome.desktop.session idle-delay 0

echo -e "${CYAN}[!] Download & install teamviewer ${NC}"
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
dpkg -i teamviewer_amd64.deb
apt-get --fix-broken install -y

echo -e "${CYAN}[!] Download & instal NVM ${NC}"
runuser -l  screenmi -c 'curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash'
runuser -l  screenmi -c 'source ~/.nvm/nvm.sh'

echo -e "${CYAN}[!] Install node version ${NC}"
runuser -l  screenmi -c 'source ~/.nvm/nvm.sh && nvm install v12.14.1'
runuser -l  screenmi -c 'source ~/.nvm/nvm.sh && nvm alias default v12.14.1'

echo -e "${CYAN}[!] Download ScreenMi ${NC}"
runuser -l  screenmi -c 'git clone https://github.com/MiVents/screenmi-client.git'
chown screenmi:screenmi -R screenmi-client
chmod +x /home/screenmi/screenmi-client/startup.sh

echo -e "${CYAN}[!] Install ScreenMi Plugins ${NC}"
runuser -l  screenmi -c 'cd screenmi-client && source ~/.nvm/nvm.sh && npm install'

echo -e "${CYAN}[!] Install Electron global ${NC}"
runuser -l  screenmi -c 'source ~/.nvm/nvm.sh && npm install -g electron --unsafe-perm=true'

echo -e "${CYAN}[!] Downloading Wallpaper ${NC}"
wget https://www.mivents.nl/wp-content/uploads/2020/08/screenmi_background.png -o /home/screenmi/Pictures/

echo -e "${CYAN}[!] Install ScreenMi Client on startup ${NC}"
cat << EOF > /etc/xdg/autostart/ScreenMi-client.desktop
[Desktop Entry]
Version=1.0
Name=ScreenMi Client
Comment=ScreenMi Client
Exec=xfce4-terminal -e '/home/screenmi/screenmi-client/startup.sh'
Icon=utilities-terminal
Terminal=false
Type=Application
Categories=
StartupNotify=false
OnlyShowIn=XFCE;
Path=
EOF

echo -e "${CYAN}[!] Install Unclutter (Hiding Cursor) ${NC}"
echo unclutter -idle 1 >> /etc/rc.local

echo -e "${CYAN}[!] Disable blackscreen ${NC}"
xset -dpms
xset s off

echo -e "${CYAN}[!] Reboot ${NC}"
reboot
